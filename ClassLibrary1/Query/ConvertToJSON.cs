﻿using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace ClassLibrary1.Query
{
   public class ConvertToJSON : IConvertToJSON
    {
        public string ConvertingToJson(object obj)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();

            settings.NullValueHandling = NullValueHandling.Ignore;
            string Json = JsonConvert.SerializeObject(obj, settings);
            return Json;
          
        }
    }
}

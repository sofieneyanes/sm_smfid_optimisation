﻿using ClassLibrary1.File;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Query
{
   public class Sat_Price_SMFID : ISat_Price_SMFID
    {
        public double LastTradeDate;
        public DateTime PriceDate;
        public double AlternateMarketPrice;
        public DateTime AlternateMarketPriceDate;
        public double InterestRate;
        public DateTime AccrualDate;
        public DateTime PayableDate;
        public double DividendRate;
        public DateTime RecordDate;
        public DateTime ExDividendDate;
        public double DividendYield;
        public Sat_Price_SMFID(FidelitySMFIDFileLine line)
        {
            LastTradeDate = line.LastTradePrice();
            PriceDate = line.PriceDate();
            AlternateMarketPrice = line.AlternateMarketPrice();
            AlternateMarketPriceDate = line.AlternateMarketPriceDate();
            InterestRate = line.InterestRate();
            AccrualDate = line.AccuralDate();
            PayableDate = line.PayableDate();
            DividendRate = line.DividendRate();
            RecordDate = line.RecordDate();
            ExDividendDate = line.ExDividendDate();
            DividendYield = line.DividendYield();
        }
    }
}

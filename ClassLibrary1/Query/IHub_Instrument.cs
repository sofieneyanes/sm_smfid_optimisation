﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Query
{
    public interface IHub_Instrument
    {
         string GetCode();
        string GetCodeType();
        string GetRecordSRC();
    }
}

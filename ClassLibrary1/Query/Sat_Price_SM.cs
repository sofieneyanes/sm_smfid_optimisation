﻿using ClassLibrary1.File;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Query
{
    public class Sat_Price_SM : ISat_Price_SM
    {
        public double BidPrice;
        public double AskPrice;
        public double LastTradePrice;
        public DateTime PriceDate;
        public Sat_Price_SM(FidelitySMFileLine line)
        {
            BidPrice = line.BidPrice();
            AskPrice = line.AskPrice();
            LastTradePrice = line.LastTradePrice();
            PriceDate = line.PriceDate();        }
    }
}

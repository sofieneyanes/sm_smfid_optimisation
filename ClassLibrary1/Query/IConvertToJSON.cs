﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Query
{
    public interface IConvertToJSON
    {
        string ConvertingToJson(object obj);
    }
}

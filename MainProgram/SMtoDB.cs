﻿using ClassLibrary1.File;
using ClassLibrary1.Query;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;


namespace MainProgram
{
    public interface ISMtoDB
    {
        void SendSMtoDB(IFidelityFile file, string FileName);
    }


    public class SMtoDB : ISMtoDB
    {
        public void SendSMtoDB(IFidelityFile file, string FileName)
        {

            //List<SAT_PRICE> ListSatPrice = new List<SAT_PRICE>();
            //List<SAT_INSTRUMENT> ListSatInstrument = new List<SAT_INSTRUMENT>();
            //List<HUB_PRICE> ListHubPrice = new List<HUB_PRICE>();
            //List<HUB_INSTRUMENT> ListHubInstrument = new List<HUB_INSTRUMENT>();
            //List<string> NewInstrumentCodes = ListHubPrice.Select(hub => hub.INSTRUMENT_CODE).ToList();
            //List<string> NewCodes = ListHubInstrument.Select(hub => hub.CODE).ToList();

            bool New_HUB;
            bool New_HUB2;


            Database.SetInitializer<data_vault_dev_db_01Entities2>(new CreateDatabaseIfNotExists<data_vault_dev_db_01Entities2>());


            using (var ctx = new data_vault_dev_db_01Entities2())
            {
                //try
                //{
                //    ctx.Configuration.AutoDetectChangesEnabled = false;



                List<HUB_INSTRUMENT> ExistingInstrumentHubs = ctx.HUB_INSTRUMENT.Select(m => m).ToList();
                List<HUB_PRICE> ExistingPriceHubs = ctx.HUB_PRICE.Select(m => m).ToList();
                List<SAT_PRICE> ExistingPriceSats = ctx.SAT_PRICE.Select(m => m).ToList();
                List<SAT_INSTRUMENT> ExistingInstrumentSats = ctx.SAT_INSTRUMENT.Select(m => m).ToList();


                var Code_CodeType = new List<Couple_Code>();
                var Code_CodeTypeConcatenation = new List<string>();

                var CodeInstrument_CodeType = new List<Couple_Code>();
                var CodeInstrument_CodeTypeConcatenation = new List<string>();

                var listPriceHubs = new List<HUB_PRICE>();
                var listInstrumentHubs = new List<HUB_INSTRUMENT>();




                foreach (FidelitySMFileLine line in file.List1())
                {



                    #region Preparing JSON

                    IHub_Instrument Hub_Instrument = new Hub_Instrument(line);
                    ISat_Instrument_SM Sat_Instrument_SM = new Sat_Instrument_SM(line);
                    IConvertToJSON Convert = new ConvertToJSON();
                    string Sat_Instrument_SMJSON = Convert.ConvertingToJson(Sat_Instrument_SM);
                    IHub_Price Hub_Price = new Hub_Price(line);
                    ISat_Price_SM Sat_Price_SM = new Sat_Price_SM(line);
                    string Sat_Price_SMJSON = Convert.ConvertingToJson(Sat_Price_SM);

                    HUB_INSTRUMENT HUB_INSTRUMENT = new HUB_INSTRUMENT();
                    SAT_INSTRUMENT SAT_INSTRUMENT = new SAT_INSTRUMENT();
                    HUB_PRICE HUB_PRICE = new HUB_PRICE();
                    SAT_PRICE SAT_PRICE = new SAT_PRICE();

                    HUB_PRICE.PRICE_DTS = line.PriceDate().AddHours(16);
                    HUB_PRICE.LOAD_DATE = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd hh:mm:ss:FFFF"), "yyyyMMdd hh:mm:ss:FFFF", CultureInfo.InvariantCulture);
                    HUB_PRICE.INSTRUMENT_CODE = Hub_Price.GetInstrumentCode();
                    HUB_PRICE.CODE_TYPE = Hub_Price.GetCodeType();
                    HUB_PRICE.RECORD_SRC = Hub_Price.GetRecordSRC();

                    SAT_PRICE.LOAD_DATE = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd hh:mm:ss:FFFF"), "yyyyMMdd hh:mm:ss:FFFF", CultureInfo.InvariantCulture);
                    SAT_PRICE.RECORD = Sat_Price_SMJSON;
                    SAT_PRICE.RECORD_DATE = new DateTime(Int32.Parse(FileName.Substring(2, 2)) + 2000, Int32.Parse(FileName.Substring(4, 2)), Int32.Parse(FileName.Substring(6, 2)));
                    SAT_PRICE.SATELLITE_TYPE = "SM";
                    SAT_PRICE.RECORD_SRC = "SM";

                    HUB_INSTRUMENT.RECORD_SRC = Hub_Instrument.GetRecordSRC();
                    HUB_INSTRUMENT.LOAD_DATE = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd hh:mm:ss:FFFF"), "yyyyMMdd hh:mm:ss:FFFF", CultureInfo.InvariantCulture);
                    HUB_INSTRUMENT.CODE = Hub_Instrument.GetCode();
                    HUB_INSTRUMENT.CODE_TYPE = Hub_Instrument.GetCodeType();

                    SAT_INSTRUMENT.RECORD_SRC = "SM";
                    SAT_INSTRUMENT.LOAD_DATE = DateTime.ParseExact(System.DateTime.Now.ToString("yyyyMMdd hh:mm:ss:FFFF"), "yyyyMMdd hh:mm:ss:FFFF", CultureInfo.InvariantCulture);
                    SAT_INSTRUMENT.RECORD = Sat_Instrument_SMJSON;
                    SAT_INSTRUMENT.RECORD_DATE = new DateTime(Int32.Parse(FileName.Substring(2, 2)) + 2000, Int32.Parse(FileName.Substring(4, 2)), Int32.Parse(FileName.Substring(6, 2)));
                    SAT_INSTRUMENT.SATELLITE_TYPE = "SM";


                    string InstKey = string.Concat(HUB_INSTRUMENT.CODE, HUB_INSTRUMENT.CODE_TYPE);
                    var coupleInst = new Couple_Code(HUB_INSTRUMENT.CODE, HUB_INSTRUMENT.CODE_TYPE);
                    New_HUB = !Code_CodeTypeConcatenation.Contains(InstKey);

                    if (New_HUB)
                    {
                        HUB_INSTRUMENT.SAT_INSTRUMENT.Add(SAT_INSTRUMENT);
                        listInstrumentHubs.Add(HUB_INSTRUMENT);

                        Code_CodeTypeConcatenation.Add(string.Concat(HUB_INSTRUMENT.CODE, HUB_INSTRUMENT.CODE_TYPE));
                        Code_CodeType.Add(coupleInst);

                    }
                    else
                    {
                        var listOfCoherentInstrumentSat = new List<SAT_INSTRUMENT>();
                        bool test = true;
                        var hub = new HUB_INSTRUMENT();
                        hub = listInstrumentHubs.Where(a => a.CODE == HUB_INSTRUMENT.CODE && a.CODE_TYPE == HUB_INSTRUMENT.CODE_TYPE).First();
                        listOfCoherentInstrumentSat = hub.SAT_INSTRUMENT.ToList();
                        foreach (var sat in listOfCoherentInstrumentSat)
                        {
                            if (sat.RECORD == SAT_INSTRUMENT.RECORD)
                            {
                                test = false;
                                break;
                            }
                        }
                        if (test)
                        {
                            hub.SAT_INSTRUMENT.Add(SAT_INSTRUMENT);
                        }

                    }




                    string PriceKey = string.Concat(HUB_PRICE.INSTRUMENT_CODE, HUB_PRICE.CODE_TYPE);
                    var couplePrice = new Couple_Code(HUB_PRICE.INSTRUMENT_CODE, HUB_PRICE.CODE_TYPE);
                    New_HUB2 = !CodeInstrument_CodeTypeConcatenation.Contains(PriceKey);

                    if (New_HUB2)
                    {
                        HUB_PRICE.SAT_PRICE.Add(SAT_PRICE);
                        listPriceHubs.Add(HUB_PRICE);

                        CodeInstrument_CodeTypeConcatenation.Add(string.Concat(HUB_PRICE.INSTRUMENT_CODE, HUB_PRICE.CODE_TYPE));
                        CodeInstrument_CodeType.Add(couplePrice);

                    }
                    else
                    {
                        var listOfCoherentPricetSat = new List<SAT_PRICE>();
                        bool test = true;
                        var hub = new HUB_PRICE();
                        hub = listPriceHubs.Where(a => a.INSTRUMENT_CODE == HUB_PRICE.INSTRUMENT_CODE && a.CODE_TYPE == HUB_PRICE.CODE_TYPE).First();
                        listOfCoherentPricetSat = hub.SAT_PRICE.ToList();
                        foreach (var sat in listOfCoherentPricetSat)
                        {
                            if (sat.RECORD == SAT_PRICE.RECORD)
                            {
                                test = false;
                                break;
                            }
                        }
                        if (test)
                        {
                            hub.SAT_PRICE.Add(SAT_PRICE);
                        }

                    }


                    #endregion
                }
                //Console.WriteLine("listInstrumentHubs.Count = " + listInstrumentHubs.Count);
                //Console.WriteLine("Code_CodeType.Count = " + Code_CodeType.Count);
                //Console.WriteLine("Code_CodeTypeConcatenation.Count = " + Code_CodeTypeConcatenation.Count);

                //Console.WriteLine("**************************");
                //Console.WriteLine("listPriceHubs.Count = " + listPriceHubs.Count);
                //Console.WriteLine("CodeInstrument_CodeType.Count = " + CodeInstrument_CodeType.Count);
                //Console.WriteLine("CodeInstrument_CodeTypeConcatenation.Count = " + CodeInstrument_CodeTypeConcatenation.Count);
                //Console.WriteLine(CodeInstrument_CodeTypeConcatenation[0]);
                //Console.WriteLine(CodeInstrument_CodeTypeConcatenation[1]);
                //Console.WriteLine(CodeInstrument_CodeTypeConcatenation[2]);
                #region find out the new price hubs and add them to a list
                var addPriceHubList = new List<HUB_PRICE>();
                var addInstrumentHubList = new List<HUB_INSTRUMENT>();
                var dbPriceCode = new List<Couple_Code>();
                var elmentPR = new HUB_PRICE();
                List<HUB_PRICE> db_HUBS_PR = new List<HUB_PRICE>();
                var codeListPR = new List<string>();
                foreach (var item in CodeInstrument_CodeType)
                {
                    if (!codeListPR.Contains(item.code))
                    {
                        codeListPR.Add(item.code);
                    }
                }
                Console.WriteLine("codeListPR.count" + codeListPR.Count);
                var all_db_HUBS_PR = new List<HUB_PRICE>();
                //Console.WriteLine(ctx.HUB_PRICE.Where(hub => codeListPR.Contains(hub.INSTRUMENT_CODE)).FirstOrDefault());
                //if (ctx.HUB_PRICE.Where(hub => codeListPR.Contains(hub.INSTRUMENT_CODE)).FirstOrDefault() != null)
                //{
                //all_db_HUBS_PR = ctx.HUB_PRICE.Where(hub => codeListPR.Contains(hub.INSTRUMENT_CODE)).ToList();
                //use partition of codeListIN to access to db (error when using it as a hole)
                var subList = new List<string>();
                for (int i = 0; i < codeListPR.Count; i = i + 10000)
                {
                    subList = new List<string>();
                    if (i + 10000 < codeListPR.Count)
                    {

                        subList = codeListPR.GetRange(i, 10000);
                    }
                    else
                    {
                        subList = codeListPR.GetRange(i, codeListPR.Count - i);
                    }
                    Console.WriteLine(" i=" + i + "subList.Count" + subList.Count);
                    all_db_HUBS_PR.AddRange(ctx.HUB_PRICE.Where(hub => subList.Contains(hub.INSTRUMENT_CODE)).ToList());
                }
                Console.WriteLine("all_db_HUBS_PR.Count" + all_db_HUBS_PR.Count);
                //all_db_HUBS_PR = ctx.HUB_PRICE.Where(hub => hub.INSTRUMENT_CODE == "000514000").ToList();
                //Console.WriteLine(all_db_HUBS_PR.Count);
                //}
                //else
                //{
                //    all_db_HUBS_PR = null;
                //}

                // on fait une selection sur les code_type aussi
                var hubToVerifyPriceSatSQNs = new List<long>();
                foreach (var item in CodeInstrument_CodeType)
                {
                    //db_HUBS_PR.Add(all_db_HUBS_PR.Where(hub => hub.INSTRUMENT_CODE == item.code && hub.CODE_TYPE == item.codeType).FirstOrDefault());
                    elmentPR = all_db_HUBS_PR.Where(hub => hub.INSTRUMENT_CODE == item.code && hub.CODE_TYPE == item.codeType).FirstOrDefault();
                    if (elmentPR != null)
                    {
                        db_HUBS_PR.Add(elmentPR);
                        var addition = new Couple_Code(elmentPR.INSTRUMENT_CODE, elmentPR.CODE_TYPE);
                        dbPriceCode.Add(addition);
                        //verificationSQN = db_HUBS_PR.Where(hub => hub.INSTRUMENT_CODE == item.code && hub.CODE_TYPE == item.codeType).FirstOrDefault().SQN;

                        if (!hubToVerifyPriceSatSQNs.Contains(elmentPR.SQN))
                        {

                            hubToVerifyPriceSatSQNs.Add(elmentPR.SQN);
                        }
                    }
                    else
                    {
                        var h = new HUB_PRICE();
                        h = listPriceHubs.Where(hub => hub.INSTRUMENT_CODE == item.code && hub.CODE_TYPE == item.codeType).First();
                        var s = new SAT_PRICE();
                        s = h.SAT_PRICE.ToList()[0];
                        h.SAT_PRICE.Clear();
                        h.SAT_PRICE.Add(s);
                        addPriceHubList.Add(h);
                    }
                }
                #endregion
                //
                //
                //
                //
                Console.WriteLine("db_HUBS_pr.count = " + db_HUBS_PR.Count);
                Console.WriteLine("dbPriceCode.count = " + dbPriceCode.Count);
                /*
                foreach (var item in CodeInstrument_CodeType)
                {
                    elmentPR = ctx.HUB_PRICE.Where(hub => hub.CODE_TYPE == item.codeType && hub.INSTRUMENT_CODE == item.code).FirstOrDefault();
                    if (elmentPR != null)
                    {
                        db_HUBS_PR.Add(elmentPR);
                        dbPriceCode.Add(item);
                    }
                }*/

                #region find out the new instument hubs and add them to a list
                var dbInstrumentCode = new List<Couple_Code>();
                var elmentIN = new HUB_INSTRUMENT();
                List<HUB_INSTRUMENT> db_HUBS_IN = new List<HUB_INSTRUMENT>();
                var codeListIN = new List<string>();
                foreach (var item in Code_CodeType)
                {
                    if (!codeListIN.Contains(item.code))
                    {
                        codeListIN.Add(item.code);
                    }

                }
                var all_db_HUBS_IN = new List<HUB_INSTRUMENT>();
                //use partition of codeListIN to access to db (error when using it as a hole)

                for (int i = 0; i < codeListIN.Count; i = i + 10000)
                {

                    subList = new List<string>();
                    if (i + 10000 < codeListIN.Count)
                    {

                        subList = codeListIN.GetRange(i, 10000);
                    }
                    else
                    {
                        subList = codeListIN.GetRange(i, codeListIN.Count - i);
                    }
                    Console.WriteLine(" i=" + i + "subList.Count" + subList.Count);
                    all_db_HUBS_IN.AddRange(ctx.HUB_INSTRUMENT.Where(hub => subList.Contains(hub.CODE)).ToList());
                }
                Console.WriteLine("all_db_HUBS_IN.Count" + all_db_HUBS_IN.Count);
                // on fait une selection sur les code_type aussi
                var hubToVerifyInstrumentSatSQNs = new List<long>();
                foreach (var item in Code_CodeType)
                {
                    //db_HUBS_IN .Add(all_db_HUBS_IN.Where(hub => hub.CODE==item.code&&hub.CODE_TYPE==item.codeType).First());
                    elmentIN = all_db_HUBS_IN.Where(hub => hub.CODE == item.code && hub.CODE_TYPE == item.codeType).FirstOrDefault();
                    if (elmentIN != null)
                    {
                        db_HUBS_IN.Add(elmentIN);
                        var addition = new Couple_Code(elmentIN.CODE, elmentIN.CODE_TYPE);
                        dbInstrumentCode.Add(addition);
                        if (!hubToVerifyInstrumentSatSQNs.Contains(elmentIN.SQN))
                        {
                            hubToVerifyInstrumentSatSQNs.Add(elmentIN.SQN);
                        }
                    }
                    else
                    {
                        //elmentIN=listInstrumentHubs.Where(hub => hub.CODE == item.code && hub.CODE_TYPE == item.codeType).First();
                        var h = new HUB_INSTRUMENT();
                        h = listInstrumentHubs.Where(hub => hub.CODE == item.code && hub.CODE_TYPE == item.codeType).First();
                        var s = new SAT_INSTRUMENT();
                        s = h.SAT_INSTRUMENT.ToList()[0];
                        h.SAT_INSTRUMENT.Clear();
                        h.SAT_INSTRUMENT.Add(s);
                        addInstrumentHubList.Add(h);
                    }
                }
                #endregion
                //
                //
                //
                //
                Console.WriteLine("db_HUBS_IN.count = " + db_HUBS_IN.Count);
                Console.WriteLine("dbInstrumentCode.count = " + dbInstrumentCode.Count);


                Console.WriteLine("addPriceHubList.count = " + addPriceHubList.Count);
                Console.WriteLine("hubToVerifyPriceSatSQNs.count = " + hubToVerifyPriceSatSQNs.Count);


                ctx.Set<HUB_PRICE>().AddRange(addPriceHubList);






                Console.WriteLine("addInstrumentHubList.count = " + addInstrumentHubList.Count);
                Console.WriteLine("hubToVerifyInstrumentSatSQNs.count = " + hubToVerifyInstrumentSatSQNs.Count);
                ctx.Set<HUB_INSTRUMENT>().AddRange(addInstrumentHubList);
                #region add new price sats
                if (hubToVerifyPriceSatSQNs.Count > 0)
                {
                    var subSqnList = new List<long>();
                    List<SAT_PRICE> db_SATS_PR = new List<SAT_PRICE>();
                    List<SAT_PRICE> db_SATS_PR_NotSM = new List<SAT_PRICE>();

                    for (int i = 0; i < hubToVerifyPriceSatSQNs.Count; i = i + 10000)
                    {
                        subSqnList = new List<long>();
                        if (i + 10000 < hubToVerifyPriceSatSQNs.Count)
                        {

                            subSqnList = hubToVerifyPriceSatSQNs.GetRange(i, 10000);
                        }
                        else
                        {
                            subSqnList = hubToVerifyPriceSatSQNs.GetRange(i, hubToVerifyPriceSatSQNs.Count - i);
                        }
                        //Console.WriteLine(" i=" + i + "subList.Count" + subSqnList.Count);
                        db_SATS_PR.AddRange(ctx.SAT_PRICE.Where(sat => subSqnList.Contains(sat.SQN) && sat.SATELLITE_TYPE == "SM").GroupBy(s => s.SQN).Select(s => s.OrderByDescending(sat => sat.RECORD_DATE).FirstOrDefault()).ToList());
                        db_SATS_PR_NotSM.AddRange(ctx.SAT_PRICE.Where(sat => subSqnList.Contains(sat.SQN) && sat.SATELLITE_TYPE != "SM").GroupBy(s => s.SQN).Select(s => s.OrderByDescending(sat => sat.RECORD_DATE).FirstOrDefault()).ToList());
                    }

                    var addPriceSatList = new List<SAT_PRICE>();

                    var price = new HUB_PRICE();
                    var list_of_Price_SQN_ToRemove = new List<long>();

                    foreach (var item in db_SATS_PR)
                    {
                        var toComparePR = new List<SAT_PRICE>();

                        price = db_HUBS_PR.Where(h => h.SQN == item.SQN).First();
                        list_of_Price_SQN_ToRemove.Add(item.SQN);
                        
                        toComparePR = listPriceHubs.Where(l => l.CODE_TYPE == price.CODE_TYPE && l.INSTRUMENT_CODE == price.INSTRUMENT_CODE).FirstOrDefault().SAT_PRICE.ToList();
                        foreach (var n in toComparePR)
                        {
                            if (item.RECORD_DATE < n.RECORD_DATE && item.RECORD != n.RECORD)
                            {
                                //add the chosen sats to the list of addition

                                n.SQN = item.SQN;
                                addPriceSatList.Add(n);
                                break;
                            }
                        }
                    }
                    db_SATS_PR_NotSM.RemoveAll(s => list_of_Price_SQN_ToRemove.Contains(s.SQN));
                    foreach (var item in db_SATS_PR_NotSM)
                    {
                        var toAdd = new SAT_PRICE();
                        price = db_HUBS_PR.Where(h => h.SQN == item.SQN).First();
                        toAdd = listPriceHubs.Where(l => l.CODE_TYPE == price.CODE_TYPE && l.INSTRUMENT_CODE == price.INSTRUMENT_CODE).FirstOrDefault().SAT_PRICE.First();
                        toAdd.SQN = item.SQN;
                        addPriceSatList.Add(toAdd);
                    }
                    Console.WriteLine("db_SATS_PR.Count = " + db_SATS_PR.Count);
                    Console.WriteLine("db_SATS_PR_not.Count = " + db_SATS_PR_NotSM.Count);
                    Console.WriteLine("addPriceSatList.Count = " + addPriceSatList.Count);
                    ctx.Set<SAT_PRICE>().AddRange(addPriceSatList);

                }
                #endregion
                #region add new instrument sats
                if (hubToVerifyInstrumentSatSQNs.Count > 0)
                {
                    var subSqnList = new List<long>();
                    List<SAT_INSTRUMENT> db_SATS_IN = new List<SAT_INSTRUMENT>();
                    List<SAT_INSTRUMENT> db_SATS_IN_NotSM = new List<SAT_INSTRUMENT>();
                    for (int i = 0; i < hubToVerifyInstrumentSatSQNs.Count; i = i + 10000)
                    {
                        subSqnList = new List<long>();
                        if (i + 10000 < hubToVerifyInstrumentSatSQNs.Count)
                        {

                            subSqnList = hubToVerifyInstrumentSatSQNs.GetRange(i, 10000);
                        }
                        else
                        {
                            subSqnList = hubToVerifyInstrumentSatSQNs.GetRange(i, hubToVerifyInstrumentSatSQNs.Count - i);
                        }
                        //Console.WriteLine(" i=" + i + "subList.Count" + subSqnList.Count);
                        db_SATS_IN.AddRange(ctx.SAT_INSTRUMENT.Where(sat => subSqnList.Contains(sat.SQN) && sat.SATELLITE_TYPE == "SM").GroupBy(s => s.SQN).Select(s => s.OrderByDescending(sat => sat.RECORD_DATE).FirstOrDefault()).ToList());
                        db_SATS_IN_NotSM.AddRange(ctx.SAT_INSTRUMENT.Where(sat => subSqnList.Contains(sat.SQN) && sat.SATELLITE_TYPE != "SM").GroupBy(s => s.SQN).Select(s => s.OrderByDescending(sat => sat.RECORD_DATE).FirstOrDefault()).ToList());

                    }
                    //db_SATS_IN = ctx.SAT_INSTRUMENT.Where(sat => hubToVerifyInstrumentSatSQNs.Contains(sat.SQN)).GroupBy(s => s.SQN).Select(s => s.OrderByDescending(sat => sat.RECORD_DATE).FirstOrDefault()).ToList();
                    var addInstrumentSatList = new List<SAT_INSTRUMENT>();

                    var inst = new HUB_INSTRUMENT();
                    var list_of_Instrument_SQN_ToRemove = new List<long>();
                    
                    foreach (var item in db_SATS_IN)
                    {
                        var toCompareIN = new List<SAT_INSTRUMENT>();

                        inst = db_HUBS_IN.Where(h => h.SQN == item.SQN).First();
                        list_of_Instrument_SQN_ToRemove.Add(item.SQN);
                        
                        toCompareIN = listInstrumentHubs.Where(l => l.CODE_TYPE == inst.CODE_TYPE && l.CODE == inst.CODE).FirstOrDefault().SAT_INSTRUMENT.ToList();
                        foreach (var n in toCompareIN)
                        {

                            if (item.RECORD_DATE < n.RECORD_DATE && item.RECORD != n.RECORD)
                            {
                                //add the chosen sats to the list of addition
                                n.SQN = item.SQN;
                                addInstrumentSatList.Add(n);
                                break;
                            }
                        }

                    }
                    db_SATS_IN_NotSM.RemoveAll(s => list_of_Instrument_SQN_ToRemove.Contains(s.SQN));
                    foreach (var item in db_SATS_IN_NotSM)
                    {
                        var toAdd = new SAT_INSTRUMENT();
                        inst = db_HUBS_IN.Where(h => h.SQN == item.SQN).First();
                        toAdd = listInstrumentHubs.Where(l => l.CODE_TYPE == inst.CODE_TYPE && l.CODE == inst.CODE).First().SAT_INSTRUMENT.First();
                        toAdd.SQN = item.SQN;
                        addInstrumentSatList.Add(toAdd);
                    }
                    Console.WriteLine("db_SATS_IN.Count = " + db_SATS_IN.Count);
                    Console.WriteLine("db_SATS_IN_not.Count = " + db_SATS_IN_NotSM.Count);
                    Console.WriteLine("addInstrumentSatList.Count = " + addInstrumentSatList.Count);
                    ctx.Set<SAT_INSTRUMENT>().AddRange(addInstrumentSatList);

                }
                #endregion
                Console.WriteLine("fin");
                //Console.ReadKey();
                //}
                //finally
                //{
                //    ctx.Configuration.AutoDetectChangesEnabled = true;
                //}
                ctx.SaveChanges();


            }
        }
    }
}